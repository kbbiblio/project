import { LOGIN_ROUTE, CHAT_ROUTE } from "./utilites/consts";
import Login from "./components/Login/Login";
import Chat from "./components/Chat.js/Chat";

export const publicRoutes = [
    {
        path: LOGIN_ROUTE,
        Element: Login
    }
]
export const privateRoutes = [
    {
        path: CHAT_ROUTE,
        Element: Chat
    }
]