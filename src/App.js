import "./App.css";
import React, {useState} from "react";
import BookCards from "./components/BookCards/BookCards";
import { Route, Routes } from "react-router-dom";
import { AuthContextProvider } from './components/Context/Context';
import NavMenu from "./components/Layout/NavMenu/NavMenu";
import Login from "./components/Login/Login";
import Header from "./components/Header/Header";
// import Footer from "./components/Footer/footer";
import { Container } from "react-bootstrap";
import PersArea from "./components/PersArea/PersArea";
import AdminArea from "./components/AdminArea/AdminArea";
import Footer from "./components/Footer/footer";
import Modal from "./components/Modal/Modal";
import NoMatch from "./components/NoMatch/NoMatch";
import Transition from "./components/Transition/transition";

function App() {

  const [search, setSearch] = useState("")
  const onSearch = (e) => {
    setSearch(e.target.value)
  }
  const [books, setBooks] = useState('')

  return (
    <div className="App">
      <Modal />
      <Container>
        <AuthContextProvider>
          <Header />
          <NavMenu onSearch={onSearch} search={search}/>
          <Routes>
            <Route path="/" element={<BookCards search={search} books={books}/>} />
            <Route path="/login" element={<Login />} />
            <Route path="/persarea" element={<PersArea />} />
            <Route path="/admin" element={<AdminArea />} />
            <Route path="/transition" element={<Transition />} />
            <Route path="/BookCards" element={<BookCards />} />
            <Route path="*" element={<NoMatch />} />
          </Routes>
          <Footer />
        </AuthContextProvider>
      </Container>
    </div>
  );
}

export default App
