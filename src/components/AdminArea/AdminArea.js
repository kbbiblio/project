import React, { useEffect } from "react";
import { useState } from "react";
import { db } from "../../firebase";
import {
  addDoc,
  collection,
  deleteDoc,
  doc,
  getDocs,
  updateDoc,
} from "firebase/firestore";

const AdminArea = () => {
  const [newName, setNewName] = useState("");
  const [newDesc, setNewDesc] = useState("");
  const [newAuthor, setNewAuthor] = useState("");
  const [newImg, setNewImg] = useState("");
  const [books, setBooks] = useState([]);
  const booksCollectionRef = collection(db, "booki");

  const deleteBook = async (id) => {
    const userDoc = doc(db, "booki", id);
    await deleteDoc(userDoc);
  };

  const createBook = async () => {
    await addDoc(booksCollectionRef, {
      name: newName,
      aboutbook: newDesc,
      author: newAuthor,
      img: newImg,
    });
  };
  // eslint-disable-next-line no-unused-vars
  const updateBook = async (id) => {
    const userDoc = doc(db, "booki", id);
    await updateDoc(userDoc);
  };

  useEffect(() => {
    const getBooks = async () => {
      const data = await getDocs(booksCollectionRef);
      setBooks(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getBooks();
  });
  return (
    <div>
      <input
        placeholder="Название"
        onChange={(event) => {
          setNewName(event.target.value);
        }}
      ></input>
      <input
        placeholder="Автор"
        onChange={(event) => {
          setNewAuthor(event.target.value);
        }}
      ></input>
      <input
        placeholder="Описание"
        onChange={(event) => {
          setNewDesc(event.target.value);
        }}
      ></input>
      <input
        placeholder="Обложка"
        onChange={(event) => {
          setNewImg(event.target.value);
        }}
      ></input>
      <button onClick={createBook}> Добавить книгу </button>

      {books.map((books) => {
        return (
          <div>
            <p>
              {books.name}
              <button
                onClick={() => {
                  deleteBook(books.id);
                }}
              >
                {" "}
                Удалить{" "}
              </button>
            </p>
          </div>
        );
      })}
    </div>
  );
};

export default AdminArea;

