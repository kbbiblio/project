/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import "./style.scss";
import photo from "./assets/photo.jpg";

const Section = () => {
  return (
    <section className="product">
      <div className="container">
        <div className="product-row">
          <div className="product-content">
            <h3 className="product-content-subtitle">About the library</h3>
            <p className="paragraph">
              You meet catchy headlines on the net “top 18 books that should
              read every one", "The best books of all time" and other variations
              on topic of the only correct reading list? You don't understand
              what of them really believe? We at ReadRate believe that to choose
              the main interesting books should be the readers themselves. That
              is, you yourself. So the list below is based solely on your
              preferences.
            </p>
            <a href="#">
              <button type="button" className="btn btn-outline-dark">
                read books
              </button>
            </a>
          </div>
          <div className="product-img-wrapper">
            <img src={photo} alt="" className="product-img" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Section;
