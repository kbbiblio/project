/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import "./modal.scss";
import App from "./App";

function Modal() {
  const [openModal, setOpenModal] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setOpenModal(!openModal);
    }, 8000);
  }, []);

  return (
    <div style={{ position: "relative", zIndex: "1000000" }}>
      <App open={openModal} onClose={() => setOpenModal(false)} />
    </div>
  );
}

export default Modal;
