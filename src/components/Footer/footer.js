/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import "./footer.css";
import ScrollToTop from "react-scroll-to-top";

const Footer = () => {
  return (
    <>
      <ScrollToTop smooth top="20" />
      <footer className="footer-distributed">
        <div className="footer-left">
          <h3>
            Library<span>Project</span>
          </h3>
          <p className="footer-company-name" style={{ marginTop: "100px" }}>
            Copyright © 2022 <strong>IT academy</strong>
          </p>
        </div>

        <div className="footer-center">
          <div>
            <i className="fa fa-map-marker"></i>
            <p>
              <span>Kyrgyzstan</span>
              Kara-Balta
            </p>
          </div>

          <div>
            <i className="fa fa-phone"></i>
            <p>+996 558 238 xxx</p>
          </div>
          <div>
            <i className="fa fa-envelope"></i>
            <p>
              <a href="mailto:sagar00001.co@gmail.com">xxxxxxx@gmail.com</a>
            </p>
          </div>
        </div>
        <div className="footer-right">
          <p className="footer-company-about">
            <span>About the company</span>
            "IT Academy Kara-Balta"
            <br />
            The "Top 20 Books" list consists of the books that you most often
            read and finish reading.
          </p>
          <div className="footer-icons">
            <a href="https://m.facebook.com/">
              <i className="fa fa-facebook"></i>
            </a>
            <a href="https://www.instagram.com/">
              <i className="fa fa-instagram"></i>
            </a>
            <a href="https://twitter.com/">
              <i className="fa fa-twitter"></i>
            </a>
            <a href="https://www.youtube.com/">
              <i className="fa fa-youtube"></i>
            </a>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
