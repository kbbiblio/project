/* eslint-disable no-undef */
/* eslint-disable no-const-assign */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import firebase from "../../firebase";
import { useLocation } from "react-router-dom";
import { doc, getDoc } from "firebase/firestore";
import Container from "react-bootstrap/Container";
import BookmarkAddIcon from "@mui/icons-material/BookmarkAdd";
import { Row, Col } from "react-bootstrap";
import "./transition.scss";
import { NavLink } from "react-router-dom";

const Transition = (props) => {
  const location = useLocation();
  const [data, setData] = useState({
    img: "",
    author: "",
    name: "",
    aboutbook: "",
  });
  // eslint-disable-next-line no-unused-vars
  const ref = firebase.firestore().collection("booki");
  useEffect(() => {
    const search = {};
    location.search
      .substring(1)
      .split("&")
      .forEach((el) => {
        const tmp = el.split("=");
        search[tmp[0]] = tmp[1];
      });

    const docRef = doc(firebase.firestore(), "booki", search.id);
    getDoc(docRef)
      .then((response) => {
        setData({ ...response.data(), id: response.id });
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <div>
      <Container>
        <Row>
          <Col xl={12} lg={12} md={12} sm={12} xs={12}>
            <NavLink to={`/BookCards?id`} className="linkBook">
              <button class="custom-btn btn-3">
                <span>⬅ come back</span>
              </button>
            </NavLink>
            <button
              type="button"
              class="btn btn-outline-secondary"
              onClick={() => saveBooks(el)}
              style={{ marginTop: "-6px", marginLeft: "10px" }}>
              to favorites
              <BookmarkAddIcon />
            </button>
            <div className="bookInf">
              <div>
                <img
                  src={data.img}
                  alt="Ожидание загрузки"
                  className="imgPhoto"
                />
              </div>
              <div className="text">
                <h3 className="author">
                  <b>Автор:</b> {data.author}.
                </h3>
                <h4 className="name">
                  <b>Название:</b> {data.name}.
                </h4>
                <b>Описание:</b>
                <p className="aboutBook">{data.aboutbook}</p>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
      <hr />
    </div>
  );
};

export default Transition;
