import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import firebase from "../../../firebase";
import "./NavMenu.css";
import { signInWithGoogle } from "../../../firebase";
import React, { useState } from "react";
import { logOut } from "../../../firebase";
import { Link } from 'react-router-dom';
import { UserAuth } from '../../Context/Context';

function NavMenu(props) {
  const ref = firebase.firestore().collection("booki");
  const { user, logOut } = UserAuth();

  const handleSignOut = async () => {
    try {
      await logOut()
    } catch (error) {
      console.log(error)
    }
  }
  return (
    <Navbar bg="light" expand="lg" className="navfix">
      <Container fluid className="navBar">
        <Navbar.Brand href="/">Главная</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
            <Nav.Link href="#anchor">Популярное</Nav.Link>
            <Nav.Link href="/persarea"> Личный кабинет </Nav.Link>
          </Nav>
          <Form className="d-flex">
            <input type="text" placeholder="Название книги..." onChange={props.onSearch} value={props.search}></input>

            <Button variant="outline-success" style={{ padding: "60" }}>
              Search
            </Button>
            {user ? (
              <Nav.Link href="/login">
                <button
                  type="button"
                  className="btn btn-light"
                  // onClick={handleClick}
                >
                  Войти
                </button>
              </Nav.Link>
            ) : (
              <Link to="/login">Sign in</Link>
            )}
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavMenu;
