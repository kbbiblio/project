import React from "react";
import BorderExample from "./Spinner";

const NoMatch = () => {
  return (
    <div>
      <h2 style={{ textAlign: "center" }}>
        Ошибка 404
        <br />
        Запрашиваемая страница не найдена
      </h2>
      <BorderExample />
    </div>
  );
};

export default NoMatch;
