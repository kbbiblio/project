import Spinner from "react-bootstrap/Spinner";

function BorderExample() {
  return (
    <div>
      <Spinner
        animation="border"
        style={{ display: "block", marginLeft: "auto", marginRight: "auto" }}
      />
    </div>
  );
}

export default BorderExample;
