/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-const-assign */
/* eslint-disable no-unused-vars */
import firebase, { firestore } from '../../firebase';
import './css/BookCards.scss'
import Carousel from "react-elastic-carousel";
import React, { useEffect, useState } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Section from '../Section/Section'
import BookmarkAddIcon from '@mui/icons-material/BookmarkAdd';
import { NavLink } from "react-router-dom";
import Developments from '../Dev/dev';

function BookCards(props) {
    const ref = firebase.firestore().collection('booki')
    const books = []
    const [data, setdata] = useState([])
    const [filtered, setFiltered] = useState([]);
    const [loader, setloader] = useState(true);


    function getData() {
        ref.onSnapshot((QuerySnapshot) => {
            const items = []
            QuerySnapshot.forEach((doc) => {
                items.push(doc.data())
            })
            setFiltered(items)
            setdata(items)
            setloader(false)
        })
    }

    useEffect(() => {
        getData()
    }, [])

    useEffect(() => {
        if (!props.search) {
            setFiltered(data)
        } else {
            setFiltered(data.filter((elem) => {
                return (elem.name.toLowerCase().includes(props.search.toLowerCase()) || elem.author.toLowerCase().includes(props.search.toLowerCase()))
            }))
        }
    }, [props.search])

    const breakPoints = [
        { width: 1, itemsToShow: 1 },
        { width: 350, itemsToShow: 2 },
        { width: 568, itemsToShow: 3 },
        { width: 1100, itemsToShow: 4 },
    ];

    const saveBooks = (el) => {
        const storage = localStorage.getItem(props.name);
        if (storage) books = JSON.parse(storage);

        const idxOfExist = books.findIndex(b => JSON.stringify(b) === JSON.stringify(el));
        if (idxOfExist >= 0) {
            books.splice(idxOfExist, 1);
        } else {
            books.push(el);
        }
        localStorage.setItem(props.name, JSON.stringify(books));
    }


    return (
        <>
            <Container className="cards" >
                <h1 class="heading"> <span>Главные книги</span> </h1>
                <Row style={{ marginTop: "50px" }} class='col-3 col-md-3 col-lg-3'>
                    {loader === false &&
                        (filtered.map((el) => {
                            return (
                                <Col className="blockBook">
                                    <img src={el.img} alt='' className='bookCardsImg' />
                                    <hr style={{ color: "#252525" }} />
                                    <h5 className='adaptive'>{el.author}</h5>
                                    <h4 className='adaptive'><b>{el.name}</b></h4>
                                        <button type="button" class="btn btn-secondary" onClick={() => saveBooks(el)} style={{ height: '90%' }}><BookmarkAddIcon /></button>
                                    <div>
                                        <NavLink to={`/transition?id=${el.id}`} className='linkBook'>
                                            <button type='button' class="btn btn-outline-primary">to read</button>
                                        </NavLink>
                                    </div>
                                </Col>
                            )
                        }))}
                </Row>
            </Container>
            <hr style={{ color: "#252525" }} />
            <div className='bookSlider' >
                <h2 className='populyar' id='anchor' style={{ textAlign: "center" }}>Популярное</h2>
                <Carousel breakPoints={breakPoints}>
                    {data.map((book) => (
                        <carousel><img src={book.img} alt=''></img></carousel>
                    ))}
                </Carousel>
            </div>
            <Section />
            <Developments />
        </>
    )
}


export default BookCards;
