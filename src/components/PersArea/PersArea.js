import React from "react";
import { UserAuth } from "../Context/Context";
import "./PersArea.css";

const Account = (props) => {
  const { logOut, user } = UserAuth();

  const handleSignOut = async () => {
    try {
      await logOut();
    } catch (error) {
      console.log(error);
    }
  };

  const ArrBooks = JSON.parse(localStorage.getItem(props.name));
  const DeleteBook = (el) => {
    const filteredBooks = ArrBooks.filter((el) => el !== el);
    console.log(JSON.parse((filteredBooks)));
  };

  return (
    <>
      <div className="w-[300px] m-auto">
        <h1 className="text-center text-2xl font-bold pt-12">Account</h1>
        <div>
          <p>Welcome, {user?.displayName}</p>
        </div>
        <button onClick={handleSignOut} className="border py-2 px-5 mt-10">
          Logout
        </button>
      </div>
      <div className="favcards">
        {ArrBooks.map((el) => {
          for (let i in el) {
            return (
              <div className="_favcards">
                <img src={el.img} />
                <p>{el.name}</p>
                <button onClick={DeleteBook}>Удалить</button>
              </div>
            );
          }
        })}
      </div>
    </>
  );
};

export default Account;
