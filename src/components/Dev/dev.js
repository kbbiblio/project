/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import "./dev.scss";
import Baiel from "./assets/Baiel.png";
import Khurshed from "./assets/Khurshed.png";

const Developments = () => {
  return (
    <>
      <h3 className="process__subtitle" style={{ marginTop: "30px" }}>
        Developments
      </h3>
      <div class="wrapper">
        <div class="block">
          <div class="process__item">
            <img src={Baiel} alt="" className="img" />
            <br />
            <h4 class="info__title">Baiel</h4>
          </div>
        </div>
        <div class="block">
          <div class="process__item">
            <img src={Khurshed} alt="" className="img" />
            <br />
            <h4 class="info__title"> Khurshed</h4>
          </div>
        </div>
      </div>
    </>
  );
};

export default Developments;
